	# Proof by induction

## Auf Deutsch

Ein Induktionsbeweis wird immer in zwei Schritten durchgeführt, dem Induktionsanfang und dem Induktionsschritt.

Diese werden wie folgt durchgeführt:

1. Induktionsanfang: Zeige, dass die Aussage für den Anfang, also der kleinsten Zahl n~0~ gilt
2. Induktionsschritt: Zeige, dass wenn die Aussage für ein beliebiges k > n~0~ gilt, dann muss die Aussage auch für den Nachfolger k + 1 gelten.



## Beispiel

Es gilt zu zeigen, dass:
$$
\sum^n_{i=1} i^2 = \frac{1}{6} n (n+1) (2n+1) 
$$
Für den Induktionsschritt setzen wir nun `1`, da dies der kleinste Wert für `n` ist.

Wir führen nun die folgende Nebenrechnung durch:

<img src="Proof by induction [Calculus].assets/image-20211018150731198.png" alt="image-20211018150731198" style="zoom:25%;" />

Diese Nebenrechnung können wir nun schöner darstellen, als eine folge von Gleichungen. Dann sieht der fertige Induktionsanfang wie folgt aus:

<img src="Proof by induction [Calculus].assets/image-20211018151255491.png" alt="image-20211018151255491" style="zoom:25%;" />



Für die Hypothese im Induktionsschritt schreiben wir wie folgt:

<img src="Proof by induction [Calculus].assets/image-20211018151352527.png" alt="image-20211018151352527" style="zoom:33%;" />

Für die Ausführung des Induktionsschritt fangen wir mit der linken Seite von `k+1` an und führen diese auf die rechte Seite über in dem wir die Aussage aus `k` integrieren.

Wir beginnen zunächst mit der Integration von `k` in `k+1`:

<img src="Proof by induction [Calculus].assets/image-20211018151952209.png" alt="image-20211018151952209" style="zoom:50%;" />

Jetzt führen wir wieder eine Nebenrechnung, die wir später zusammenfügen.

Wir wollen nun das Zwischenergbnis wie oben und die rechte Seite von `k+1` einzeln vereinfachen. Ziel ist es, solange zu vereinfachen, bis beider Formeln das gleiche zeigen:

<img src="Proof by induction [Calculus].assets/image-20211018152215391.png" alt="image-20211018152215391" style="zoom:25%;" />

Beide Ergebnisse können wir nun in unserem Induktionsschritt vereinigen.

Damit ist der Beweis fertig. Hier ist das Ergebnis:

<img src="Proof by induction [Calculus].assets/image-20211018150107276.png" alt="image-20211018150107276" style="zoom: 25%;" />

<img src="Proof by induction [Calculus].assets/image-20211018150217337.png" alt="image-20211018150217337" style="zoom:25%;" />

